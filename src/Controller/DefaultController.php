<?php

namespace App\Controller;

use mikehaertl\pdftk\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DefaultController extends AbstractController
{
    public function generatePdf(Request $request, ValidatorInterface $validator): Response
    {
        $input = $request->request->all();

        $groups = new Assert\GroupSequence(['Default', 'custom']);

        $constraint = new Assert\Collection([
            'lastname' => new Assert\NotBlank(),
            'firstname' => new Assert\NotBlank(),
            'birthplace' => new Assert\NotBlank(),
            'birthdate' => new Assert\Date(),
            'address' => new Assert\NotBlank(),
            'city' => new Assert\NotBlank(),
            'zipcode' => new Assert\Range(['min' => 1009, 'max' => 98833]),
            'authorization_date' => new Assert\Date(),
            'authorization_time' => new Assert\Time(),
            'authorization_reason' => new Assert\Range(['min' => 1, 'max' => 8])
        ]);

        $violations = $validator->validate($input, $constraint, $groups);

        if ($violations->count() > 0) {
            $errors = '';
            foreach ($violations as $violation) {
                $errors .= (string) $violation . "\n";
            }

            return new Response($errors);
        }

        $certificateFields = [
            'Nom Prénom' => $request->request->get('lastname').' '.$request->request->get('firstname'),
            'Date de naissance' => $request->request->get('birthdate'),
            'Lieu de naissance' => $request->request->get('birthplace'),
            'Adresse du domicile' => $request->request->get('address').' '.$request->request->get('zipcode').' '.$request->request->get('city'),
            'Lieu d\'établissement du justificatif' => $request->request->get('city'),
            'Date' => $request->request->get('authorization_date'),
            'Heure' => $request->request->get('authorization_time'),
            'distinction Motif ' . $request->request->get('authorization_reason') => 'Oui'
        ];

        $pdf = new Pdf(dirname(__DIR__) . '/../private/certificate.pdf');
        $pdf->fillForm($certificateFields)
            ->needAppearances()
            ->saveAs(sys_get_temp_dir() . '/filled.pdf')
        ;

        return $this->file(sys_get_temp_dir() . '/filled.pdf');
    }
}
