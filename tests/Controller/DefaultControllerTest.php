<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testItRespondsToPostRequest(): void
    {
        $client = self::createClient();
        $client->request('POST', '/');

        $this->assertSame($client->getResponse()->getStatusCode(), 200);
    }

    public function testAllFieldsAreRequired(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            []
        );

        $fields = [
            'lastname',
            'firstname',
            'birthplace',
            'birthdate',
            'address',
            'city',
            'zipcode',
            'authorization_date',
            'authorization_time',
            'authorization_reason'
        ];

        foreach ($fields as $field) {
            $this->assertFieldIsRequired($client->getResponse()->getContent() ?: '', $field);
        }
    }

    private function assertFieldIsRequired(string $response, string $fieldName): void
    {
        $this->assertIsInt(
            strpos($response, "Array[$fieldName]:\n    This field is missing.")
        );
    }

    public function testBirthdateShouldBeAValidDate(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['birthdate' => 'not a valid date']
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[birthdate]:\n    This value is not a valid date."
            )
        );

        $client->request(
            'POST',
            '/',
            ['birthdate' => '1970-01-01']
        );

        $this->assertFalse(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[birthdate]:\n    This value is not a valid date."
            )
        );
    }

    public function testAuthorizationDateShouldBeAValidDate(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['authorization_date' => 'not a valid date']
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_date]:\n    This value is not a valid date."
            )
        );

        $client->request(
            'POST',
            '/',
            ['authorization_date' => '1970-01-01']
        );

        $this->assertFalse(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_date]:\n    This value is not a valid date."
            )
        );
    }

    public function testAuthorizationTimeShouldBeAValidTime(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['authorization_time' => 'not a valid time']
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_time]:\n    This value is not a valid time."
            )
        );

        $client->request(
            'POST',
            '/',
            ['authorization_time' => '12:00:00']
        );

        $this->assertFalse(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_time]:\n    This value is not a valid time."
            )
        );
    }

    public function testZipcodeShouldBeAValidNumber(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['zipcode' => 'not a number']
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[zipcode]:\n    This value should be a valid number."
            )
        );
    }

    public function testZipcodeShouldBeInTheRangeOfCounty(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['zipcode' => 12]
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[zipcode]:\n    This value should be between 1009 and 98833."
            )
        );

        $client->request(
            'POST',
            '/',
            ['zipcode' => 100000]
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[zipcode]:\n    This value should be between 1009 and 98833."
            )
        );

        $client->request(
            'POST',
            '/',
            ['zipcode' => 75001]
        );

        $this->assertFalse(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[zipcode]:\n    This value should be between 1009 and 98833."
            )
        );
    }

    public function testAuthorizationReasonShouldBeAValidNumber(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['authorization_reason' => 'not a number']
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_reason]:\n    This value should be a valid number."
            )
        );
    }

    public function testAuthorizationReasonShouldBeInTheRangeOfValidReason(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            ['authorization_reason' => 0]
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_reason]:\n    This value should be between 1 and 8."
            )
        );

        $client->request(
            'POST',
            '/',
            ['authorization_reason' => 9]
        );

        $this->assertIsInt(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_reason]:\n    This value should be between 1 and 8."
            )
        );

        $client->request(
            'POST',
            '/',
            ['authorization_reason' => 1]
        );

        $this->assertFalse(
            strpos(
                $client->getResponse()->getContent() ?: '',
                "Array[authorization_reason]:\n    This value should be between 1 and 8."
            )
        );
    }

    public function testItGenerateACertificate(): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/',
            [
                'firstname' => 'Camille',
                'lastname' => 'Dupont',
                'birthdate' => '1970-01-01',
                'birthplace' => 'Paris',
                'address' => '999 avenue de France',
                'city' => 'Paris',
                'zipcode' => 75001,
                'authorization_date' => '2021-01-01',
                'authorization_time' => '12:00:00',
                'authorization_reason' => 1
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/pdf');
        $this->assertResponseHeaderSame('content-disposition', 'attachment; filename=filled.pdf');
        $this->assertFileExists(sys_get_temp_dir() . '/filled.pdf');
    }
}
