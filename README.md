# Attestation utile

Attestation utile est une application mobile pour générer des attestations de sortie développée avec Flutter.
Ce dépôt est le backend [Symfony](https://symfony.com/) qui permet de générer les attestations au format pdf
grâce à l'exécutable [Pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/).

## Installation

Installer l'exécutable pdftk

```bash
apt install pdftk
```

Et les dépendances

```bash
composer install
```

## Utilisation

Exemple de requête avec `curl`

```bash
curl --location --request POST 'https://url_du_serveur.com' \
--form 'firstname="Camille"' \
--form 'lastname="Dupont"' \
--form 'birthdate="1970-12-24"' \
--form 'birthplace="Paris"' \
--form 'address="999 avenue de France"' \
--form 'city="Paris"' \
--form 'zipcode="75001"' \
--form 'authorization_date="2020-12-24"' \
--form 'authorization_time="00:00:00"' \
--form 'authorization_reason="1"'
```

`authorization_reason` correspond à l'index du motif de sortie choisi parmis ceux présents sur l'attestation source.

La réponse à cette requête sera l'attestation au format pdf.

## Remerciement

- Ce projet utilise [mikehaertl/php-pdftk](https://github.com/mikehaertl/php-pdftk) pour intéragir avec Pdftk
